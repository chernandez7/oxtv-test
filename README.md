# xotv-test

## Instructions to run locally

- `cd xotv-test`
- `npm i` or `yarn`
- `npm start` or `yarn start`

`.env` file required in root folder. Example provided below

```
SKIP_PREFLIGHT_CHECK=true
REACT_APP_UNSPLASH_ACCESS=<YOUR_UNSPLASH_ACCESS_KEY_HERE>
REACT_APP_UNSPLASH_SECRET=<YOUR_UNSPLASH_SECRET_KEY_HERE>
REACT_APP_UNSPLASH_CALLBACK=urn:ietf:wg:oauth:2.0:oob
```

## Demo

[![Image from Gyazo](https://i.gyazo.com/dbbdd24929ce54eff8e32f6a988f55d6.gif)](https://gyazo.com/dbbdd24929ce54eff8e32f6a988f55d6)
