import React from "react";
import { HomeScreen } from "./Screens";
import "./App.css";

const App = () => <HomeScreen />;

export default App;
