import React from "react";
import { Sidebar, Grid, Footer } from "../../Components";
import "./HomeScreen.css";

class HomeScreen extends React.Component {
  state = {
    photos: [],
    selectedUser: ""
  };

  updatePhotos = photos => {
    this.setState({ photos });
  };

  updateSelectedUser = selectedUser => {
    this.setState({ selectedUser });
  };

  render() {
    const { photos, selectedUser } = this.state;
    return (
      <div className="App">
        <Sidebar
          updatePhotos={this.updatePhotos}
          updateSelectedUser={this.updateSelectedUser}
        />
        <Grid photos={photos} selectedUser={selectedUser} />
        <Footer />
      </div>
    );
  }
}

export default HomeScreen;
