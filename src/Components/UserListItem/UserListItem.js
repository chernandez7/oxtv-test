/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import PropTypes from "prop-types";
import "./UserListItem.css";

const UserListItem = ({ getUserImages, user: { username, bio, photos } }) => (
  <a onClick={() => getUserImages(username)}>
    <div className="user-list-item">
      {photos[0] && (
        <img
          src={photos[0].urls.thumb}
          alt="user-thumb"
          className="user-photo"
        />
      )}
      <p>{username}</p>
      <p>{bio}</p>
    </div>
  </a>
);

UserListItem.propTypes = {
  getUserImages: PropTypes.func,
  user: PropTypes.object
};

UserListItem.defaultProps = {
  getUserImages: () => {},
  user: {}
};

export default UserListItem;
