import React from "react";
import PropTypes from "prop-types";
import "./Photo.css";

const Photo = ({ photo: { description, title, tags, preview_photos } }) => (
  <div className="photo">
    {preview_photos[0].urls && (
      <img src={preview_photos[0].urls.thumb} alt={title} />
    )}
    <p>{title}</p>
    <p>{description}</p>
    {/* tags.map(tag => (
      <span key={tag.title}>{`${tag.title} `}</span>
    )) */}
  </div>
);

Photo.propTypes = {
  photo: PropTypes.object
};

Photo.defaultProps = {
  photo: {}
};

export default Photo;
