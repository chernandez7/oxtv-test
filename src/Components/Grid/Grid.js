import React from "react";
import PropTypes from "prop-types";
import { Photo } from "..";
import "./Grid.css";

const Grid = ({ photos, selectedUser }) => (
  <div className="grid">
    {selectedUser !== "" && photos.length > 0 ? (
      <h1>{selectedUser}</h1>
    ) : selectedUser !== "" && !photos.length > 0 ? (
      <h1> {`${selectedUser} doesn't have any uploaded images :(`}</h1>
    ) : (
      <div />
    )}
    {photos.length > 0 ? (
      photos.map(pic => <Photo key={pic.id} photo={pic} />)
    ) : (
      <div />
    )}
  </div>
);

Grid.propTypes = {
  photos: PropTypes.array,
  selectedUser: PropTypes.string
};

Grid.defaultProps = {
  photos: [],
  selectedUser: ""
};

export default Grid;
