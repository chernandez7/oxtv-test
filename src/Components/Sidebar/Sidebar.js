import React from "react";
import PropTypes from "prop-types";
import { UserListItem } from "..";
import "./Sidebar.css";

const Sidebar = ({
  searchTerm,
  updateSearchTerm,
  searchforUserByTerm,
  users,
  getUserImages,
  hasSearched
}) => (
  <div className="sidebar">
    <div className="form-container">
      <input
        className="search-input"
        value={searchTerm}
        onChange={e => {
          updateSearchTerm(e.target.value);
          searchforUserByTerm(e.target.value);
        }}
        placeholder="Enter a search term..."
      />
      {/* <button
        className="search-button"
        type="button"
        onClick={() => searchforUserByTerm(searchTerm)}
      >
        Search
        </button> */}
    </div>
    {users.length > 0 ? (
      users.map(user => (
        <UserListItem
          key={user.username}
          user={user}
          getUserImages={getUserImages}
        />
      ))
    ) : searchTerm !== "" && hasSearched ? (
      <p>{`No users match the term "${searchTerm}"`}</p>
    ) : (
      <div />
    )}
  </div>
);

Sidebar.propTypes = {
  searchTerm: PropTypes.string,
  updateSearchTerm: PropTypes.func,
  searchforUserByTerm: PropTypes.func,
  users: PropTypes.arrayOf(PropTypes.object),
  getUserImages: PropTypes.func,
  hasSearched: PropTypes.bool
};

Sidebar.defaultProps = {
  searchTerm: "",
  updateSearchTerm: () => {},
  searchforUserByTerm: () => {},
  users: [],
  getUserImages: () => {},
  hasSearched: false
};

export default Sidebar;
