import React from "react";
import PropTypes from "prop-types";
import Unsplash, { toJson } from "unsplash-js";
import Sidebar from "./Sidebar";

class SidebarContainer extends React.Component {
  state = {
    searchTerm: "",
    users: [],
    hasSearched: false
  };

  componentDidMount() {
    const unsplash = new Unsplash({
      applicationId: process.env.REACT_APP_UNSPLASH_ACCESS,
      secret: process.env.REACT_APP_UNSPLASH_SECRET,
      callbackUrl: process.env.REACT_APP_UNSPLASH_CALLBACK
    });
    this.unsplash = unsplash;
  }

  updateSearchTerm = searchTerm => {
    const { updateSelectedUser, updatePhotos } = this.props;
    this.setState({ searchTerm, hasSearched: false });
    updateSelectedUser("");
    updatePhotos([]);
  };

  getUserImages = username => {
    const { updatePhotos, updateSelectedUser } = this.props;
    updateSelectedUser(username);
    try {
      this.unsplash.users
        .collections(username, 1)
        .then(toJson)
        .then(json => {
          updatePhotos(json);
        });
    } catch (err) {
      console.log(err);
    }
  };

  searchforUserByTerm = term => {
    try {
      this.unsplash.search
        .users(term, 1)
        .then(toJson)
        .then(json => {
          const { results /* total, total_pages */ } = json;
          this.setState({ users: results, hasSearched: true });
        });
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { searchTerm, users, hasSearched } = this.state;
    return (
      <Sidebar
        searchTerm={searchTerm}
        updateSearchTerm={this.updateSearchTerm}
        searchforUserByTerm={this.searchforUserByTerm}
        users={users}
        getUserImages={this.getUserImages}
        hasSearched={hasSearched}
      />
    );
  }
}

SidebarContainer.propTypes = {
  updatePhotos: PropTypes.func,
  updateSelectedUser: PropTypes.func
};

SidebarContainer.defaultProps = {
  updatePhotos: () => {},
  updateSelectedUser: () => {}
};

export default SidebarContainer;
